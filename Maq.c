#include <stdio.h>
typedef enum
{
    goNORTE,
    waitNORTE,
    goESTE,
    waitESTE
}
Estados;
typedef enum
{ 	
    NO_HAY_CARROS,
    CARROS_DEL_ESTE,
    CARROS_DEL_NORTE,
    CARROS_AMBOS_LADOS
}Entradas;

static int Estado_Actual_G = 0;

void Muestra_Estado_Actual(int EdoActual)
{
    switch (EdoActual)
    {
        case goNORTE:
            printf("goNORTE");
            break;
        case waitNORTE:
            printf("waitNORTE");
            break;
        case goESTE:
            printf("goESTE");
            break;
        case waitESTE:
            printf("waitESTE");
            break;
    }
};

void Maquina(int Transicion, int Estado_Actual)
{
    switch (Estado_Actual)
    {
        case goNORTE:
            if(Transicion == NO_HAY_CARROS || Transicion == CARROS_DEL_NORTE)
            {
                Estado_Actual_G = goNORTE;
            }
            else
            {
                Estado_Actual_G = waitNORTE;
            }
            break;
        case waitNORTE:
            Estado_Actual_G = goESTE;
            break;
        case goESTE:
            if (Transicion == NO_HAY_CARROS || Transicion == CARROS_DEL_ESTE)
            {
                Estado_Actual_G = goESTE;
            }
            else
            {
                Estado_Actual_G = waitESTE;
            }
            break;
        case waitESTE:
            Estado_Actual_G = goNORTE;
            break;
    }
    
    Muestra_Estado_Actual(Estado_Actual_G);
};

int main()
{
    int Lista_Transiciones[6] ={CARROS_DEL_ESTE, CARROS_DEL_ESTE, CARROS_DEL_NORTE, CARROS_AMBOS_LADOS, NO_HAY_CARROS, CARROS_AMBOS_LADOS};
    printf("\nInicio [");
    Muestra_Estado_Actual(Estado_Actual_G);
    printf("]");
    for (int i = 0; i < 6; i++)
    {
        printf("\nEstado Actual [");
        Maquina(Lista_Transiciones[i], Estado_Actual_G);
        printf("]");
    }
    printf("\n\n");
    return 0;
}