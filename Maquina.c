#include <stdio.h>
typedef enum
{
    goNORTE, 	waitNORTE, 	goESTE, 	waitESTE
}Estados;

typedef enum
{
    NO_HAY_CARROS, 	CARROS_DEL_ESTE, 	CARROS_DEL_NORTE, 	CARROS_AMBOS_LADOS
}Entradas;

static int Estado_Actual_G = 0;
int Maquina(int Transicion, int Estado_Actual)
{
    switch (Estado_Actual)
    {
        case goNORTE:
            if (Transicion == NO_HAY_CARROS || Transicion == CARROS_DEL_NORTE)
            {
                Estado_Actual_G = goNORTE;
                }
                else
                {
                    Estado_Actual_G = waitNORTE;
                }
            }
            break;
        case waitNORTE:
            Estado_Actual_G = goESTE;
            break;
        case goESTE:
            if (Transicion == NO_HAY_CARROS || Transicion == CARROS_DEL_ESTE)
            {
                Estado_Actual_G = goESTE;
            }
            else
            {
                Estado_Actual_G = waitESTE;
            }
            break;
        case waitESTE:
            Estado_Actual_G = goNORTE;
            break;
    }
    return Estado_Actual_G;
}