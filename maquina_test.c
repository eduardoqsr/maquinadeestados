#include "Maquina.c"
#include <gtest/gtest.h>

TEST(Estado_0, Maquina_0)
{
    ASSERT_EQ(goNORTE, Maquina(NO_HAY_CARROS,goNORTE));
}

TEST(Estado_1, Maquina_1)
{
    ASSERT_EQ(goNORTE, Maquina(CARROS_DEL_NORTE,goNORTE));
}

TEST(Estado_2, Maquina_2)
{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_DEL_ESTE,goNORTE));
}

TEST(Estado_3, Maquina_3)
{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_AMBOS_LADOS,goNORTE));
}

TEST(Estado_4, Maquina_4)
{
    ASSERT_EQ(goNORTE, Maquina(NO_HAY_CARROS,goNORTE));
}

TEST(Estado_5, Maquina_5)
{
    ASSERT_EQ(goNORTE, Maquina(CARROS_DEL_NORTE,goNORTE));
}

TEST(Estado_6, Maquina_6)
{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_DEL_ESTE,goNORTE));
}

TEST(Estado_7, Maquina_7)
{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_AMBOS_LADOS,goNORTE));
}

TEST(Estado_8, Maquina_8)
{
    ASSERT_EQ(goNORTE, Maquina(NO_HAY_CARROS,goNORTE));
}

TEST(Estado_9, Maquina_9)
{
    ASSERT_EQ(goNORTE, Maquina(CARROS_DEL_NORTE,goNORTE));
}

TEST(Estado_10, Maquina_10)

{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_DEL_ESTE,goNORTE));
}

TEST(Estado_11, Maquina_11)
{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_AMBOS_LADOS,goNORTE));
}

TEST(Estado_12, Maquina_12)
{
    ASSERT_EQ(goNORTE, Maquina(NO_HAY_CARROS,goNORTE));
}

TEST(Estado_13, Maquina_13)
{
    ASSERT_EQ(goNORTE, Maquina(CARROS_DEL_NORTE,goNORTE));
}

TEST(Estado_14, Maquina_14)
{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_DEL_ESTE,goNORTE));
}

TEST(Estado_15, Maquina_15)
{
    ASSERT_EQ(waitNORTE, Maquina(CARROS_AMBOS_LADOS,goNORTE));
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}